As part of the Scholastic interview process for Drupal software engineering positions,
we require that every candidate clones this git repository, completes the requested
exercises, commit your work to a local repository, and send the repository to us.

Setup
-----
1. Clone the Interview repository onto your development environment.
2. Setup the Drupal installation using the scholastic\_interview.sql dump located in the sql directory.
3. Configure the Drupal installation to point to the proper database on your machine.
4. The Drupal installation username is admin and the password is admin.

Required Work
-------------
1. Create tests for the requirements below in the tests directory using PHPUnit.
2. Add the following fields to the Book Content Type:
- Field name: ISBN-13
- Type: Textfield
3. Update the ct\_book content type Feature module and commit this to the repository.
4. Add three Book nodes with test data.
5. Create a new View named Homepage Book Blocks with the following requirements:
As a customer, I want to see 3 featured books on the homepage. The block shall 
contain the Book Title, Summary Description (trimmed to 100 chars), and the ISBN-13 under the title.
The title of the block should be Featured Books. Only books with Promoted to Front selected should display in the block.
6. Create a Context to place the newly created block on the homepage in the left sidebar region.
7. Create a Feature module for the newly created View and Context called Homepage Book Blocks
and commit this to the repository in the site/all/modules/features directory.
8. Ensure that all PHPUnit tests created earlier pass and meet the requirements.
9. Add your resume to the docs directory.
10. Zip your local copy of the repository with all code and resume committed and email the zip to the requesting interviewer.

