<?php

/**
 * Test that the number of characters displayed meet the requirements.
 * 
 * Requirements:
 * Book Title: =< 25 characters
 * Description: =< 100 characters
 * ISBN-13: = 13 characters
 */
